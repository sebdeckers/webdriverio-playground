# webdriverio-playground 🏫

This was done on Windows 10. 🤕 YMMV.

## Selenium Standalone Server

1. Install Java, Edge, Chrome, & Firefox on the system.

1. Download into the `./selenium` subdirectory:

   1. [Selenium Standalone Server](http://www.seleniumhq.org/download/)

   1. [geckodriver](https://github.com/mozilla/geckodriver/releases)

   1. [ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/home)

   1. [Microsoft WebDriver](https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/)

1. Run:

   ```
   java -jar -Dwebdriver.gecko.driver=./selenium/geckodriver.exe -Dwebdriver.ie.driver=./selenium/MicrosoftWebDriver.exe -Dwebdriver.chrome.driver=./selenium/chromedriver.exe ./selenium/selenium-server-standalone-3.1.0.jar
   ```

## Run Mocha Tests in WDIO

```
./node_modules/.bin/wdio wdio.conf.js
```

## Edge

The Microsoft WebDriver for Edge still refers to its browser name as `internet explorer`.
